package at.project.uno;

public class Main {

	public static void main(String[] args) {
		System.out.println("Ein neues Spiel beginnt.");

		Runde runde = new Runde();
		runde.addSpieler(new Spieler("Valentina"));
		runde.addSpieler(new Spieler("Lisa"));
		runde.addSpieler(new Spieler("Corinna"));
		runde.addSpieler(new Spieler("Bianca"));

		for (Farbe farbe : Farbe.values()) {
			for (int i = 0; i < 10; i++) {
				String s = String.valueOf(i);
				if (i == 0) {
					runde.addKarte(new Karte(i, s, farbe));
				} else {
					runde.addKarte(new Karte(i, s, farbe));
					runde.addKarte(new Karte(i, s, farbe));
				}

			}
			for (int i = 0; i < 2; i++) {
				runde.addKarte(new Karte(20, "PLUS ZWEI", farbe));
				runde.addKarte(new Karte(20, "RETOUR", farbe));
				runde.addKarte(new Karte(20, "AUSSETZEN", farbe));

			}
			runde.addKarte(new Karte(50, "PLUS VIER"));
			runde.addKarte(new Karte(50, "FARBWAHL"));
		}

		runde.shuffle();

		runde.deal();
		runde.firstCard();

		while (true) {
			runde.nextPlayer();
		}
	}
}
