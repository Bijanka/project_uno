package at.project.uno;

public class Karte {

	public int score;

	public String wert;

	public Farbe farbe;

	public Karte(int score, String wert, Farbe farbe) {
		super();
		this.score = score;
		this.wert = wert;
		this.farbe = farbe;
	}

	public Karte(int score, String wert) {
		super();
		this.score = score;
		this.wert = wert;

	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getWert() {
		return wert;
	}

	public void setWert(String wert) {
		this.wert = wert;
	}

	public Farbe getFarbe() {
		return farbe;
	}

	public void setFarbe(Farbe farbe) {
		this.farbe = farbe;
	}

	public String toString() {
		if (this.getFarbe() == null) {
			return "[" + wert + "]";
		}
		return "[" + wert + "][" + farbe + "]";
	}

}
