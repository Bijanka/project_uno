package at.project.uno;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Runde {

	private List<Spieler> spieler = new ArrayList<Spieler>();

	private List<Karte> deck = new ArrayList<Karte>();

	private List<Karte> gespielt = new ArrayList<Karte>();

	private Integer aktuellerSpieler;

	private Integer gespeicherterSpieler;

	public List<Spieler> getSpieler() {
		return spieler;
	}

	public void addKarte(Karte karte) {
		// System.out.println("F�ge Karte : " + karte.toString() + " hinzu");
		this.deck.add(karte);
	}

	public void setSpieler(List<Spieler> spieler) {
		this.spieler = spieler;
	}

	public void addSpieler(Spieler spieler) {
		this.spieler.add(spieler);
	}

	public void firstCard() {

		gespielt.add(deck.remove(0));
		while (gespielt.get(0).getScore() > 9) {
			gespielt.add(deck.remove(0));
		}
		System.out.println("Die erste Karte ist: " + gespielt.get(0));
	}

	public void shuffle() {
		Collections.shuffle(deck);
		System.out.println("Das Deck besteht aus " + deck.size() + " Karten");
		System.out.println("Das Deck wurde gemischt.");
	}

	public void deal() {
		for (int i = 0; i < 7; i++) {
			for (Spieler spieler : spieler) {
				Karte karte = deck.get(0);
				System.out.println(spieler.getName() + " bekommt Karte : " + karte.toString());
				spieler.addKarte(karte);
				deck.remove(0);
			}

		}
	}

	public void nextPlayer() {
		if (aktuellerSpieler == null) {
			aktuellerSpieler = 0;
		} else {
			aktuellerSpieler++;
			if (aktuellerSpieler > (spieler.size() - 1)) {
				aktuellerSpieler = 0;
			}
		}

		System.out.println(spieler.get(aktuellerSpieler).getName() + " ist als n�chstes dran.");
		// status methode
		System.out.println("Du hast die Karten:");
		int card = 1;
		for (Karte karte : spieler.get(aktuellerSpieler).getKarten()) {

			System.out.println(card + ") " + karte.toString());
			card++;
		}
		// user input anfordern
		Scanner scanner = new Scanner(System.in);
		String input = null;
		System.out.println("Welche Karte m�chtest du spielen?");
		input = scanner.nextLine();
		System.out.println("Es wird die Karte : " + input + " gespielt");
		Karte letzteKarte = gespielt.get(gespielt.size() - 1);
		//

		System.out.println("Letzte Karte: " + letzteKarte);
		int inputZahl = Integer.parseInt(input) - 1;
		Karte zuSpielen = spieler.get(aktuellerSpieler).getKarten().get(inputZahl);

		if ((letzteKarte.getFarbe() != zuSpielen.getFarbe())
				&& (!letzteKarte.getWert().equalsIgnoreCase(zuSpielen.getWert()))) {

			System.out.println("Diese Karte ist nicht m�glich.");

			input = "q";
		}

		if ((zuSpielen.getWert().equalsIgnoreCase("AUSSETZEN") && letzteKarte.getFarbe() == zuSpielen.getFarbe())
				|| (zuSpielen.getWert().equalsIgnoreCase("AUSSETZEN")
						&& letzteKarte.getWert().equalsIgnoreCase(zuSpielen.getWert()))) {

			aktuellerSpieler = aktuellerSpieler + 1;
			if (aktuellerSpieler > 3)
				aktuellerSpieler = 0;
			System.out.println(spieler.get(aktuellerSpieler).getName() + " muss aussetzen.");

		}

		if ((zuSpielen.getWert().equalsIgnoreCase("RETOUR") && letzteKarte.getFarbe() == zuSpielen.getFarbe())
				|| (zuSpielen.getWert().equalsIgnoreCase("RETOUR")
						&& letzteKarte.getWert().equalsIgnoreCase(zuSpielen.getWert()))) {
			gespeicherterSpieler = aktuellerSpieler;
			Collections.reverse(spieler);
			aktuellerSpieler = gespeicherterSpieler - 1;
			if (aktuellerSpieler < 0) {
				aktuellerSpieler = 3;
			}
			System.out.println(spieler.get(aktuellerSpieler).getName());
			System.out.println("Die Reihenfolge wird umgedreht.");

		}
		if ((zuSpielen.getWert().equalsIgnoreCase("PLUS ZWEI") && letzteKarte.getFarbe() == zuSpielen.getFarbe())
				|| (zuSpielen.getWert().equalsIgnoreCase("PLUS ZWEI")
						&& letzteKarte.getWert().equalsIgnoreCase(zuSpielen.getWert()))) {
			gespeicherterSpieler = aktuellerSpieler + 1;
			if (gespeicherterSpieler > 3)
				gespeicherterSpieler = 0;
			// spieler.get(gespeicherterSpieler).getKarten().add(deck);
			Karte karte = deck.get(0);
			deck.remove(0);
			karte = deck.get(0);
			deck.remove(0);

		}
		gespielt.add(zuSpielen);
		spieler.get(aktuellerSpieler).getKarten().remove(zuSpielen);
		System.out.println("Auf dem Tisch liegt die Karte: " + gespielt.get(gespielt.size() - 1));
		if (!input.equals("q")) {
			System.out.println("N�chster Spieler.");

		}

	}
}
