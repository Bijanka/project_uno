package at.project.uno;

import java.util.ArrayList;
import java.util.List;

public class Spieler {

    public String name;

    public List<Karte> karten = new ArrayList<Karte>();

    public int score = 0;

    public Spieler(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addKarte(Karte karte) {
        this.karten.add(karte);
    }

    public List<Karte> getKarten() {
        return karten;
    }
}
